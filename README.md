# bash-tutor

BASH Shell alapozó

## Git BASH

A [Git BASH](https://git-scm.com/downloads) egy multiplatform (tehát több féle operációs rendszeren futtatható) terminál emulátor program.

Terminal, shell, konzol, command-line (parancs-sor) mind-mind szononímák; azokat a programokat értjük alatta, amik egy karakter sorokból álló képernyőn (vagy ablakon) keresztül engednek utasításokat adni az operációs rendszer magjának (core vagy kernel) és ugyan ott jelenítik meg a válaszait.